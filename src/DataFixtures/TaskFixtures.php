<?php

namespace App\DataFixtures;

use App\Entity\Task;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;

class TaskFixtures extends Fixture implements DependentFixtureInterface, FixtureGroupInterface
{
    public const ADMIN_TASK_REFERENCE = 'admin-task';
    public const USER_TASK_REFERENCE = 'user-task';
    /**
     * @param ObjectManager $manager
     * @throws \Exception
     */
    public function load(ObjectManager $manager)
    {

        /**
         * Création d'une nouvelle tâche d'une heure et 30 minutes...
         */

        /**
         * @var User $user Le créateur de la tâche
         */
        $user = $this->getReference(UserFixtures::USER_REFERENCE);

        $userTask = new Task();
        $userTask->setCreatedBy($user);
        $userTask->setName("Test1");
        $userTask->setStartAt(new \DateTime('now'));
        $userTask->setDuration(new \DateInterval("PT1H30M"));

        /**
         * On ajoute une référence pour l'assignement des tâches
         */
        $this->addReference(self::USER_TASK_REFERENCE, $userTask);

        /**
         * On met à jour les données
         */
        $manager->persist($userTask);


        /**
         * Création d'un deuxième tâche. Elle commence à son initialisation et termine le 2019-03-04 à 16:40
         */

        /**
         * @var User $admin Le créateur de la tâche
         */
        $admin = $this->getReference(UserFixtures::ADMIN_REFERENCE);

        $adminTask = new Task();
        $adminTask->setCreatedBy($admin);
        $adminTask->setName("Test2");
        $adminTask->setStartAt(new \DateTime('now'));
        $adminTask->setDurationWithEndDate(new \DateTime('2019-03-04 16:40'));

        /**
         * On ajoute une référence pour l'assignement des tâches
         */
        $this->addReference(self::ADMIN_TASK_REFERENCE, $adminTask);

        /**
         * On met à jour les données
         */
        $manager->persist($adminTask);

        /**
         * Et on exécute
         */
        $manager->flush();
    }

    /**
     * Nous avons besoin d'utilisateurs pour créer des tâches, alors on ajoute les fixtures des utilisateurs comme dépendance
     *
     * @return array
     */
    public function getDependencies(): array
    {
        return [UserFixtures::class];
    }

    public static function getGroups(): array
    {
        return ['TaskFixtures'];
    }
}
