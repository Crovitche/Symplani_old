<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\Factory;

class UserFixtures extends Fixture implements FixtureGroupInterface
{
    /** Constantes pour le référencement */
    public const ADMIN_REFERENCE = 'admin';
    public const USER_REFERENCE = 'user';

    private $passwordEncoder;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }

    /**
     * @param ObjectManager $manager
     * @throws \Exception
     */
    public function load(ObjectManager $manager)
    {
        $faker = Factory::create('fr_CH');

        for($i = 0; $i < 10; $i++)
        {
            $randomUser = new User();
            $randomUser->setEmail($faker->unique()->safeEmail);
            $randomUser->setRoles(['ROLES_USER']);
            $randomUser->setPassword($this->passwordEncoder->encodePassword(
                $randomUser,
                'Admin$00'
            ));

            $manager->persist($randomUser);
        }

        $user = new User();
        $user->setEmail('aje@rpn.ch');
        $user->setRoles(['ROLES_USER']);
        $user->setPassword($this->passwordEncoder->encodePassword(
            $user,
            'Admin$00'
        ));

        $this->addReference(self::USER_REFERENCE, $user);

        $manager->persist($user);

        $admin = new User();
        $admin->setEmail('contact@thibaultg.info');
        $admin->setRoles(['ROLES_ADMIN']);
        $admin->setPassword($this->passwordEncoder->encodePassword(
            $admin,
            'Admin$00'
        ));

        $this->addReference(self::ADMIN_REFERENCE, $admin);

        $manager->persist($admin);
        $manager->flush();

    }

    public static function getGroups(): array
    {
        return ['UserFixtures'];
    }
}
