<?php

namespace App\DataFixtures;

use App\Entity\Task;
use App\Entity\User;
use App\Entity\UsersTasks;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;

class UsersTasksFixtures extends Fixture implements DependentFixtureInterface, FixtureGroupInterface
{
    public function load(ObjectManager $manager)
    {
        // Prenons un cas concret où un utilisateur créerait une tâche à un utilisateur lambda

        /**
         * @var  Task  $task La tâche
         */
        $task = $this->getReference(TaskFixtures::ADMIN_TASK_REFERENCE);

        /**
         * @var User $user L'utilisateur
         */
        $user = $this->getReference(UserFixtures::USER_REFERENCE);

        $assignment = new UsersTasks();

        $assignment->assign($user,$task);

        $manager->persist($assignment);
        $manager->flush();
    }

    public function getDependencies(): array
    {
        return [UserFixtures::class, TaskFixtures::class];
    }

    public static function getGroups(): array
    {
        return ['UsersTasksFixtures'];
    }
}
