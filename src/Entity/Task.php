<?php

namespace App\Entity;

use App\Entity\User;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Le code à l'intérieur du constructeur est important: La propriété $tasks doit être un objet collection qui
 * implémente l'interface de collection doctrine. Dans notre cas, une ArrayCollection est utilisé. Elle a
 * l'air et se comporte comme un tableau, mais il y a un peu plus de flexibilité. Imaginons que ce soit un
 * tableau...
 */
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

/**
 * Définition du repository des tâches
 * @ORM\Entity(repositoryClass = "App\Repository\TaskRepository")
 *
 * Le nom de la table dans la base de données et la contrainte d'integrité
 * @ORM\Table(name = "task",
 *     uniqueConstraints = {
 *         @ORM\UniqueConstraint(
 *             name = "UK1_task",
 *             columns = {
 *                 "name",
 *                 "start_at",
 *                 "duration"
 *             }
 *         )
 *     })
 *
 * Pour avoir des champs updated_at et created_at
 * @ORM\HasLifecycleCallbacks()
 *
 *
 * La partie "Symfony" pour la validation
 * @UniqueEntity(
 *     fields={
 *         "name",
 *         "startAt",
 *         "duration"
 *     },
 *     errorPath = "name",
 *     message = "Il ne peut pas y avoir deux tâches avec le même nom commençant à la même date/heure."
 * )
 */
class Task
{
    // <editor-fold desc="Déclarations de variables (tout fonctionne)">
    /**
     * @var  integer  $id  La clef primaire de la tâche
     *
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type = "integer",
     *             options = {"unsigned" : true})
     */
    private $id;

    /**
     * @var  string  $name  Le nom de la tâche
     *
     * @ORM\Column(type = "string",
     *             length = 180,
     *             nullable = false)
     */
    private $name;

    /**
     * @var  \DateTime  $startAt  La date/heure de début de la tâche
     *
     * @ORM\Column(type = "datetime",
     *             nullable = false)
     */
    private $startAt;

    /**
     * @var  \DateInterval  $duration  La durée de la tâche
     *
     * @ORM\Column(type = "dateinterval",
     *             nullable = false)
     */
    private $duration;

    /**
     * @var  \DateTime  $createdAt  La date de création de la tâche
     *
     * @ORM\Column(type = "datetime",
     *             name = "created_at",
     *             nullable = false)
     */
    private $createdAt;

    /**
     * @var  \DateTime  $updatedAt  La date de dernière mise à jour de la tâche
     *
     * @ORM\Column(type = "datetime",
     *             name = "updated_at",
     *             nullable = false)
     */
    private $updatedAt;

    /**
     * @var  User  Le créateur de la tâche
     *
     * Les tâche a un créateur
     * @ORM\ManyToOne(targetEntity = "User")
     *
     * Le champ faisant référence au champ "id" de la table Task s'appelle user_id.
     * Si un utilisateur est supprimé, la tâche ne sera pas supprimé, son créateur
     * sera définit à NULL.
     * @ORM\JoinColumn(name="creator_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $createdBy;
    //</editor-fold>

    /**
     * @var Collection|User  $users  les utilisateurs attribués à cette tâche
     *
     * @ORM\OneToMany(targetEntity = "UsersTasks", mappedBy = "task_id")
     */
    private $users;

    /**
     * Constructeur de tâche
     */
    public function __construct()
    {
        $this->users = new ArrayCollection();
    }

    // <editor-fold defaultstate="collapsed" desc="Getters / Setters">

    /**
     * Retourne l'id de la tâche
     *
     * @return int
     */
    public function getId(): int
    {
        return (int) $this->id;
    }

    /**
     * Retourne le nom de la tâche
     *
     * @return string
     */
    public function getName(): string
    {
        return (string) $this->name;
    }

    /**
     * Définit le nom de la tâche
     *
     * @param   string  $name  le nom de la tâche
     * @return  Task
     */
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Retourne la date (avec l'heure) du début de la tâche
     *
     * @return \DateTimeInterface
     */
    public function getStartAt(): \DateTimeInterface
    {
        return $this->startAt;
    }

    /**
     * Définit la date (avec l'heure) du début de la tâche
     *
     * @param \DateTimeInterface $startAt
     * @return Task
     */
    public function setStartAt(\DateTimeInterface $startAt): self
    {
        $this->startAt = $startAt;

        return $this;
    }

    /**
     * Retourne la durée de la tâche
     *
     * @return \DateInterval
     */
    public function getDuration(): \DateInterval
    {
        return $this->duration;
    }

    /**
     * Définit la durée de la tâche
     *
     * @param  \DateInterval  $duration  The duration of the task
     * @return  Task
     */
    public function setDuration(\DateInterval $duration): self
    {
        $this->duration = $duration;

        return $this;
    }

    /**
     * Définit la durée de la tâche avec la date de fin
     * TODO: Faire une règle de validation pour vérifier que la date de fin et plus grande que celle du début
     *
     * @param \DateTime $endDate
     * @return Task
     */
    public function setDurationWithEndDate(\DateTime $endDate): self
    {
        if($endDate > $this->startAt)
        {
            $this->duration = date_diff($this->startAt, $endDate);
        }

        return $this;
    }

    /**
     * Retourne la date de fin de la tâche
     *
     * @return  \DateTime
     */
    public function getEndDate(): \DateTime
    {
        return $this->startAt->add($this->duration);
    }

    /**
     * Obtient le créateur de la tâche
     *
     * @return User
     */
    public function getCreatedBy(): User
    {
        return $this->createdBy;
    }

    /**
     * Définit le créateur de la tâche
     *
     * @param \App\Entity\User $user
     * @return Task
     */
    public function setCreatedBy(User $user): self
    {
        $this->createdBy = $user;
        return $this;
    }

    // </editor-fold>

    /**
     * Retourne la liste des utilisateurs de la tâche
     *
     * @return Collection|User[]
     */
    public function getUsers(): Collection
    {
       return $this->users;
    }

    // <editor-fold desc="Méthodes venant de ORM\HasLifecycleCallbacks()">

    /**
     * Se déclenche avant chaque insertion
     *
     * @throws \Exception
     * @ORM\PrePersist()
     *
     * @return void
     */
    public function OnPrePersist(): void
    {
        $now = new \DateTime('now');
        $this->createdAt = $now;
        $this->updatedAt = $now;
    }

    /**
     * Se déclenche avant chaque mise à jour
     *
     * @throws \Exception
     * @ORM\PreUpdate()
     *
     * @return void
     */
    public function OnPreUpdate(): void
    {
        $this->updatedAt = new \DateTime("now");
    }

    // </editor-fold>
}
