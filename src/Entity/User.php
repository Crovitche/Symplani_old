<?php

namespace App\Entity;

use App\Entity\Task as Task;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;


/**
 * Le code à l'intérieur du constructeur est important: La propriété $tasks doit être un objet collection qui
 * implémente l'interface de collection doctrine. Dans notre cas, une ArrayCollection est utilisé. Elle a
 * l'air et se comporte comme un tableau, mais il y a un peu plus de flexibilité. Imaginons que ce soit un
 * tableau...
 */
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

/**
 * @ORM\Entity(repositoryClass = "App\Repository\UserRepository")
 * @ORM\Table(name = "user")
 */
class User implements UserInterface
{
    /**
     * @var  integer  La clef primaire de l'utilisateur
     *
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type = "integer",
     *             options = {"unsigned" : true})
     */
    private $id;

    /**
     * @var  string  L'adresse email de l'utilisateur
     *
     * @ORM\Column(type = "string",
     *             length = 180,
     *             nullable = false,
     *             unique = true)
     */
    private $email;

    /**
     * @var  array  Les rôles de l'utilisateur (en json dans la base de données)
     *
     * @ORM\Column(type = "json",
     *             nullable = false)
     */
    private $roles = [];

    /**
     * @var  string  Le mot de passe hashé
     *
     * @ORM\Column(type = "string",
     *             length = 180,
     *             nullable = false)
     */
    private $password;

    /**
     * @ORM\OneToMany(targetEntity="UsersTasks", mappedBy="user_id")
     */
    private $tasks;

    /**
     * Constructeur d'utilisateur
     */
    public function __construct()
    {
        $this->tasks = new ArrayCollection();
    }

    /**
     * Retourne la clef primaire de l'utilisateur
     *
     * @return int
     */
    public function getId(): int
    {
        return (int) $this->id;
    }

    /**
     * Retourne l'adresse email de l'utilisateur
     *
     * @return string
     */
    public function getEmail(): string
    {
        return (string) $this->email;
    }

    /**
     * Définit l'adresse email de l'utilisateur
     *
     * @param  string  $email  L'adresse email de l'utilisateur
     * @return User
     */
    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string) $this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    /**
     * Définit les rôles de l'utilisateur
     *
     * @param array $roles
     * @return User
     */
    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return (string) $this->password;
    }

    /**
     * Définit le mot de passe de l'utilisateur
     *
     * @param string $password
     * @return User
     */
    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @return Collection|Task[]
     */
    public function getTasks(): Collection
    {
        return $this->tasks;
    }

    /**
     * @see UserInterface
     */
    public function getSalt()
    {
        // not needed when using the "bcrypt" algorithm in security.yaml
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }
}
