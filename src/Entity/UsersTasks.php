<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UsersTasksRepository")
 * @ORM\Table(name="users_tasks")
 */
class UsersTasks
{
    /**
     * @ORM\Column(name="user_id", type="integer")
     * @ORM\Id()
     * @ORM\ManyToOne(targetEntity = "User")
     * @ORM\JoinColumn(nullable = false)
     */
    private $user_id;

    /**
     * @ORM\Column(name="task_id", type="integer")
     * @ORM\Id()
     * @ORM\ManyToOne(targetEntity = "Task", inversedBy = "task_id")
     * @ORM\JoinColumn(nullable = false)
     */
    private $task_id;

    /**
     * Spécifie si la tâche est terminé ou pas
     *
     * @var  boolean  $isCompleted
     *
     * @ORM\Column(type = "boolean",
     *             name = "is_completed",
     *             nullable = true,
     *             options = {
     *                 "default" : null
     *             })
     */
    private $isCompleted;

    // <editor-fold desc="Getters / Setter">

    /**
     * Retourne l'id de l'utilisateur
     *
     * @return int
     */
    public function getUserId(): int
    {
        return $this->user_id;
    }

    /**
     * Définit l'id de l'utilisateur
     *
     * @param int $user_id
     * @return UsersTasks
     */
    public function setUserId(int $user_id): self
    {
        $this->user_id =  $user_id;

        return $this;
    }

    /**
     * Retourne l'id de la tâche
     *
     * @return int
     */
    public function getTaskId(): int
    {
        return $this->task_id;
    }

    /**
     * Définit l'id de la tâche
     *
     * @param int $task_id
     * @return UsersTasks
     */
    public function setTaskId(int $task_id): self
    {
        $this->task_id = $task_id;

        return $this;
    }

    /**
     * Retourne le statut (si terminé ou pas) de la tâche
     *
     * @return bool|null
     */
    public function getIsCompleted(): ?bool
    {
        return $this->isCompleted;
    }

    /**
     * Définit le statut de la tâche (si terminé ou pas)
     *
     * @param bool $isCompleted
     * @return UsersTasks
     */
    public function setIsCompleted(bool $isCompleted = null): self
    {
        $this->isCompleted = $isCompleted;

        return $this;
    }
    // </editor-fold>

    public function assign(User $user, Task $task): self
    {
        $this->user_id = $user->getId();
        $this->task_id = $task->getId();

        return $this;
    }
}
