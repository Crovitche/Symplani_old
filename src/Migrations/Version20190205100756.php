<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190205100756 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE users_tasks ADD user_id INT NOT NULL, ADD task_id INT NOT NULL, DROP user_id_id, DROP task_id_id, CHANGE completed is_completed TINYINT(1) DEFAULT NULL, ADD PRIMARY KEY (user_id, task_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE users_tasks DROP PRIMARY KEY');
        $this->addSql('ALTER TABLE users_tasks ADD user_id_id INT UNSIGNED NOT NULL, ADD task_id_id INT UNSIGNED NOT NULL, DROP user_id, DROP task_id, CHANGE is_completed completed TINYINT(1) DEFAULT NULL');
    }
}
